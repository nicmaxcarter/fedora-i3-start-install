#!/bin/bash

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# this only works for a user 'mcarter'
# for a different user, change 'mcarter' references in this file
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


# INITIAL VARIABLES
# ----------------------------------------------------------------------------

# current directory of git project
gitdir=$(pwd)

# move to the main user directory before running scripts
cd ~
# directory of config files
currentdir=$(pwd)
configdir="$currentdir/.config"

# ----------------------------------------------------------------------------
# end INITIAL VARIABLES

# ----------------------------------------------------------------------------
# SECTION FOUR - appearance
# ----------------------------------------------------------------------------

# change terminal appearance
cp $gitdir/.Xresources ~/.Xresources
xrdb .Xresources
echo ""

# copy i3 config file
sudo cp -f $gitdir/i3config/config $configdir/i3/config

# set up wallpaper
cp $gitdir/walls ~/walls -r

# set login wallpaper
originalbg="usr/share/backgrounds/default.png"
newbg="home/mcarter/temp/start/walls/minimal.png"
sudo sed -i "s|$originalbg|$newbg|g" /etc/lightdm/lightdm-gtk-greeter.conf

# turn off terminal beep
originalbeep="#set bell-style none"
newbeep="set bell-style none"
sudo sed -i "s|$originalbeep|$newbeep|g" /etc/inputrc
exit

# ----------------------------------------------------------------------------
# END SECTION FOUR
# ----------------------------------------------------------------------------
