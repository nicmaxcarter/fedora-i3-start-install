#!/bin/bash

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# this only works for a user 'mcarter'
# for a different user, change 'mcarter' references in this file
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


# INITIAL VARIABLES
# ----------------------------------------------------------------------------

# current directory of git project
gitdir=$(pwd)

# move to the main user directory before running scripts
cd ~
# directory of config files
currentdir=$(pwd)
configdir="$currentdir/.config"

# ----------------------------------------------------------------------------
# end INITIAL VARIABLES

# ----------------------------------------------------------------------------
# SECTION SIX - other packages I like
# ----------------------------------------------------------------------------

# terminal ui for git
brew install jesseduffield/lazygit/lazygit

# cleanup brew because it takes up so much damn space
brew cleanup -n

# add flathub
#flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo -y

#flatpak install flathub in.cinny.Cinny

#rust and cargo
sudo dnf install rust cargo -y

# tod for todoist command line
#cargo install tod

appsdir="$gitdir/apps";

# app image launcher
sudo dnf install -y "$appsdir/appimagelauncher-2.2.0-travis995.0f91801.x86_64.rpm"

# todoist app image
todoist="$appsdir/Todoist-linux-x86_64-8.9.2.AppImage"
sudo cp $todoist "$currentdir/Applications/Todoist-linux-x86_64-8.9.2.AppImage"


# ----------------------------------------------------------------------------
# end SECTION SIX
# ----------------------------------------------------------------------------

echo ""
echo "Done: Please reboot"
