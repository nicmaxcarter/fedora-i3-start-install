
message="IS THIS REALLY IMPORTANT !!???"

#spd-say "Max, '$message'"

declare -a messages

messages[0]=$message
messages[1]="NO, SERIOUSLY, THINK ABOUT IT !!???"
messages[2]="$message?"
messages[3]="WHAT ARE YOU DOING WITH YOUR LIFE !!???"
messages[4]="$message!"

for value in "${messages[@]}"
do
    notify-send "$value"
done
