#!/bin/bash

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# this only works for a user 'mcarter'
# for a different user, change 'mcarter' references in this file
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


# INITIAL VARIABLES
# ----------------------------------------------------------------------------

# current directory of git project
gitdir=$(pwd)

# move to the main user directory before running scripts
cd ~
# directory of config files
currentdir=$(pwd)
configdir="$currentdir/.config"

# ----------------------------------------------------------------------------
# end INITIAL VARIABLES

# ----------------------------------------------------------------------------
# SECTION ONE - initial packages
# ----------------------------------------------------------------------------

# remove existing firefox to start fresh
sudo dnf remove -y firefox

# update everything first
sudo dnf update -yy

# rpm fusion
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf update -yy

# php repository
sudo dnf install http://rpms.remirepo.net/fedora/remi-release-38.rpm -y

# designate php 8.1
sudo dnf module enable php:remi-8.1 -y

# install necessary packages
sudo dnf install -y rxvt-unicode
sudo dnf install -y git
sudo dnf install -y curl
sudo dnf install -y httpie
sudo dnf install -y composer
sudo dnf install -y firefox
sudo dnf install -y chromium
sudo dnf install -y neofetch fastfetch
sudo dnf install -y gcc-c++
sudo dnf install -y make
sudo dnf install -y sqlitebrowser
sudo dnf install -y adwaita-gtk2-theme

sudo dnf install -y vlc
sudo dnf install -y torbrowser-launcher
sudo dnf install -yy flatpak
sudo dnf install -y thunderbird
sudo dnf install -y gnome-maps
sudo dnf install -y gnome-font-viewer
sudo dnf install -y gimp
sudo dnf install -y inkscape
sudo dnf install -y libreoffice
sudo dnf install -y gnome-calculator
sudo dnf install -y gnucash # Finance management application
sudo dnf install -y gnome-boxes # virtualbox alternative
sudo dnf install -y obs-studio # screen recording
sudo dnf install -y ristretto # Image-viewer for the Xfce desktop environment
sudo dnf install -y simple-scan # Simple scanning utility

sudo dnf install -y playerctl # Command-line MPRIS-compatible Media Player Controller
sudo dnf install -y speech-dispatcher-utils # Various utilities for speech-dispatcher
#sudo dnf install -y pulseaudio --allowerasing # Improved Linux Sound Server
sudo dnf install -y xrdb # to reload .Xresources and change terminal view
sudo dnf install -y redshift # Adjusts the color temperature of your screen according to time of day
sudo dnf install -y htop # task manager
sudo dnf install -y powertop # power consumption manager
sudo dnf install -y glances # another task manager
sudo dnf install -y evince # pdf viewer
sudo dnf install -y okular # pdf viewer
sudo dnf install -y ranger # navigate files in terminal
sudo dnf install -y feh # simple photo viewer
sudo dnf install -y qutebrowser # lightweight browser
sudo dnf install -y 'mozilla-fira-*' # A nice font family
sudo dnf install -y neovim # my preference over vim
sudo dnf install -y mariadb # forget about MySQL
sudo dnf install -y mariadb-server
sudo dnf install -y plantuml # don't use it often, but I need uml and diagrams
sudo dnf install -y ImageMagick # need this for most of my projects
sudo dnf install -y ripgrep # for searching files in neovim
sudo dnf install -y fd-find # also for searching files in neovim
sudo dnf install -y lxappearance
sudo dnf install -y lxsession
sudo dnf install -y light # control backlight
sudo dnf install -y shutter # screenshots
sudo dnf install -y s3cmd # manage s3 buckets from command line
sudo dnf install -y zeal # documentation
sudo dnf install -y marker # GTK 3 markdown editor
sudo dnf install -y cloc # Count lines of code
sudo dnf install -y ncdu # Text-based disk usage viewer
sudo dnf install -y lshw # Hardware lister
sudo dnf install -y lshw-gui # Graphical hardware lister
sudo dnf install -y gh # GitHub’s official command line tool
sudo dnf install -y blueman bluez # bluetooth
sudo dnf install -y audacity # audio editing
sudo dnf install -y solaar # Device manager for a wide range of Logitech devices

#sudo dnf install -y spotify-client

# ----------------------------------------------------------------------------
# END SECTION ONE
# ----------------------------------------------------------------------------

