#!/bin/bash

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# this only works for a user 'mcarter'
# for a different user, change 'mcarter' references in this file
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


# INITIAL VARIABLES
# ----------------------------------------------------------------------------

# current directory of git project
gitdir=$(pwd)

# move to the main user directory before running scripts
cd ~
# directory of config files
currentdir=$(pwd)
configdir="$currentdir/.config"

# ----------------------------------------------------------------------------
# end INITIAL VARIABLES

# ----------------------------------------------------------------------------
# SECTION TWO - so many things unfortunately need node js
# ----------------------------------------------------------------------------

# install node
# this should install node v18
sudo yum install https://rpm.nodesource.com/pub_18.x/nodistro/repo/nodesource-release-nodistro-1.noarch.rpm -y
sudo yum install nodejs -y --setopt=nodesource-nodejs.module_hotfixes=1

# ----------------------------------------------------------------------------
# END SECTION TWO
# ----------------------------------------------------------------------------

