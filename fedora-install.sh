#!/bin/bash

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# this only works for a user 'mcarter'
# for a different user, change 'mcarter' references in this file
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


# INITIAL VARIABLES
# ----------------------------------------------------------------------------

# current directory of git project
gitdir=$(pwd)

# move to the main user directory before running scripts
cd ~
# directory of config files
currentdir=$(pwd)
configdir="$currentdir/.config"

# ----------------------------------------------------------------------------
# end INITIAL VARIABLES

# SETUP
# ----------------------------------------------------------------------------

# define bash config
bashfile="$currentdir/.bashrc";
newbashfile="$gitdir/.bashrc";

# add contents to bash file
sudo tee < $newbashfile -a $bashfile

cp "$gitdir/scripts/screensm.sh" "$currentdir/screensm.sh";
cp "$gitdir/scripts/screenlg.sh" "$currentdir/screenlg.sh";
cp "$gitdir/scripts/isitimportant.sh" "$currentdir/isitimportant.sh";

mkdir "$currentdir/Applications";
# ----------------------------------------------------------------------------
# end SETUP


# ----------------------------------------------------------------------------
# SECTION ONE - initial packages
# ----------------------------------------------------------------------------

# remove existing firefox to start fresh
sudo dnf remove -y firefox

# update everything first
sudo dnf update -yy

# rpm fusion
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf update -yy

# php repository
sudo dnf install http://rpms.remirepo.net/fedora/remi-release-38.rpm -y

# designate php 8.1
sudo dnf module enable php:remi-8.1 -y

# install necessary packages
sudo dnf install -y rxvt-unicode
sudo dnf install -y git
sudo dnf install -y curl
sudo dnf install -y httpie
sudo dnf install -y composer
sudo dnf install -y firefox
sudo dnf install -y chromium
sudo dnf install -y neofetch fastfetch
sudo dnf install -y gcc-c++
sudo dnf install -y make
sudo dnf install -y sqlitebrowser
sudo dnf install -y adwaita-gtk2-theme

sudo dnf install -y vlc
sudo dnf install -y torbrowser-launcher
sudo dnf install -yy flatpak
sudo dnf install -y thunderbird
sudo dnf install -y gnome-maps
sudo dnf install -y gnome-font-viewer
sudo dnf install -y gimp
sudo dnf install -y inkscape
sudo dnf install -y libreoffice
sudo dnf install -y gnome-calculator
sudo dnf install -y gnucash # Finance management application
sudo dnf install -y gnome-boxes # virtualbox alternative
sudo dnf install -y obs-studio # screen recording
sudo dnf install -y ristretto # Image-viewer for the Xfce desktop environment
sudo dnf install -y simple-scan # Simple scanning utility

sudo dnf install -y playerctl # Command-line MPRIS-compatible Media Player Controller
sudo dnf install -y speech-dispatcher-utils # Various utilities for speech-dispatcher
#sudo dnf install -y pulseaudio --allowerasing # Improved Linux Sound Server
sudo dnf install -y xrdb # to reload .Xresources and change terminal view
sudo dnf install -y redshift # Adjusts the color temperature of your screen according to time of day
sudo dnf install -y htop # task manager
sudo dnf install -y powertop # power consumption manager
sudo dnf install -y glances # another task manager
sudo dnf install -y evince # pdf viewer
sudo dnf install -y okular # pdf viewer
sudo dnf install -y ranger # navigate files in terminal
sudo dnf install -y feh # simple photo viewer
sudo dnf install -y qutebrowser # lightweight browser
sudo dnf install -y 'mozilla-fira-*' # A nice font family
sudo dnf install -y neovim # my preference over vim
sudo dnf install -y mariadb # forget about MySQL
sudo dnf install -y mariadb-server
sudo dnf install -y plantuml # don't use it often, but I need uml and diagrams
sudo dnf install -y ImageMagick # need this for most of my projects
sudo dnf install -y ripgrep # for searching files in neovim
sudo dnf install -y fd-find # also for searching files in neovim
sudo dnf install -y lxappearance
sudo dnf install -y lxsession
sudo dnf install -y light # control backlight
sudo dnf install -y shutter # screenshots
sudo dnf install -y s3cmd # manage s3 buckets from command line
sudo dnf install -y zeal # documentation
sudo dnf install -y marker # GTK 3 markdown editor
sudo dnf install -y cloc # Count lines of code
sudo dnf install -y ncdu # Text-based disk usage viewer
sudo dnf install -y lshw # Hardware lister
sudo dnf install -y lshw-gui # Graphical hardware lister
sudo dnf install -y gh # GitHub’s official command line tool
sudo dnf install -y blueman bluez # bluetooth
sudo dnf install -y audacity # audio editing
sudo dnf install -y solaar # Device manager for a wide range of Logitech devices

#sudo dnf install -y spotify-client

# ----------------------------------------------------------------------------
# END SECTION ONE
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
# SECTION TWO - so many things unfortunately need node js
# ----------------------------------------------------------------------------

# install node
# this should install node v18
sudo yum install https://rpm.nodesource.com/pub_18.x/nodistro/repo/nodesource-release-nodistro-1.noarch.rpm -y
sudo yum install nodejs -y --setopt=nodesource-nodejs.module_hotfixes=1

# ----------------------------------------------------------------------------
# END SECTION TWO
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
# SECTION THREE - development environment
# ----------------------------------------------------------------------------

# NEOVIM
# ----------------------------------------------------------------------------
# make a config directory for nvim and make the undo dir
mkdir -p .config/nvim/undodir

# copy nvim config
cp $gitdir/nvim/init.vim ~/.config/nvim/init.vim
cp $gitdir/nvim/coc-settings.json ~/.config/nvim/coc-settings.json

# make php-doc .vim dir
mkdir -p .vim
cp $gitdir/nvim/php-doc.vim ~/.vim/php-doc.vim

# install vimplug for plugins
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# run vimplug
nvim --headless +PlugInstall +qa

# copy resize font
urxvtdir="$currentdir/.urxvt/ext"
sudo mkdir $urxvtdir -p
sudo cp $gitdir/resize-font "$currentdir/.urxvt/ext/resize-font"
# ----------------------------------------------------------------------------
# end NEOVIM

# ----------------------------------------------------------------------------
# END SECTION THREE
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
# SECTION FOUR - appearance
# ----------------------------------------------------------------------------

# change terminal appearance
cp $gitdir/.Xresources ~/.Xresources
xrdb .Xresources
echo ""

# copy i3 config file
sudo cp -f $gitdir/i3config/config $configdir/i3/config

# set up wallpaper
cp $gitdir/walls ~/walls -r

# set login wallpaper
originalbg="usr/share/backgrounds/default.png"
newbg="home/mcarter/temp/start/walls/minimal.png"
sudo sed -i "s|$originalbg|$newbg|g" /etc/lightdm/lightdm-gtk-greeter.conf

# turn off terminal beep
originalbeep="#set bell-style none"
newbeep="set bell-style none"
sudo sed -i "s|$originalbeep|$newbeep|g" /etc/inputrc
exit

# ----------------------------------------------------------------------------
# END SECTION FOUR
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
# SECTION FIVE - development packages
# ----------------------------------------------------------------------------

# install repo for php 8
sudo dnf -y install php-{cli,fpm,mysqlnd,zip,devel,gd,mbstring,curl,xml,pear,bcmath,json}
sudo dnf -y install php-opcache php-imap php-imagick php-imagick
sudo dnf -y install gearmand php-pecl-gearman
sudo dnf install -y php-soap
sudo systemctl start php-fpm
sudo systemctl enable php-fpm

# make usual directories
devdir="$currentdir/development"
mkdir -p "$devdir"
mkdir -p "$devdir/personal"
mkdir -p "$devdir/fun"
mkdir -p "$devdir/flops"
resdir="$devdir/resources"
mkdir -p "$resdir"

# directory for plantuml diagrams
cp -r "$gitdir/development/diagrams" "$devdir/diagrams"

# directory for http requests in AIN, similar to Postman
cp -r "$gitdir/development/http_requests" "$resdir/http_requests"

# directory to house reference files, such as php docs or code cheat sheets
cp -r "$gitdir/development/references" "$resdir/references"

# directory to house often used development launch scripts
cp -r "$gitdir/development/launches" "$resdir/launches"

# directory to house often used development launch scripts
cp -r "$gitdir/Xthemes" "$currentdir/Xthemes"

# directory to house adminer file, similar to PhpMyAdmin
mkdir -p "$resdir/adminer"


# install adminer with styling
cp "$gitdir/development/adminer/adminer.css" "$resdir/adminer/adminer.css"
wget -cO "$resdir/adminer/index.php" https://github.com/vrana/adminer/releases/download/v4.8.1/adminer-4.8.1.php

# install brew package manager
echo -ne '\n' | /bin/bash -c "$(sudo curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo ""
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/mcarter/.bash_profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# install ain http client
brew tap jonaslu/tools
brew install ain

# install timer for pomodoro bash script
brew install caarlos0/tap/timer

# ----------------------------------------------------------------------------
# END SECTION FIVE
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
# SECTION SIX - other packages I like
# ----------------------------------------------------------------------------

# terminal ui for git
brew install jesseduffield/lazygit/lazygit

# cleanup brew because it takes up so much damn space
brew cleanup -n

# add flathub
#flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo -y

#flatpak install flathub in.cinny.Cinny

#rust and cargo
sudo dnf install rust cargo -y

# tod for todoist command line
#cargo install tod

appsdir="$gitdir/apps";

# app image launcher
sudo dnf install -y "$appsdir/appimagelauncher-2.2.0-travis995.0f91801.x86_64.rpm"

# todoist app image
todoist="$appsdir/Todoist-linux-x86_64-8.9.2.AppImage"
sudo cp $todoist "$currentdir/Applications/Todoist-linux-x86_64-8.9.2.AppImage"


# ----------------------------------------------------------------------------
# end SECTION SIX
# ----------------------------------------------------------------------------

echo ""
echo "Done: Please reboot"
