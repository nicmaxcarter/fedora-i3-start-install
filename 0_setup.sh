#!/bin/bash

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# this only works for a user 'mcarter'
# for a different user, change 'mcarter' references in this file
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


# INITIAL VARIABLES
# ----------------------------------------------------------------------------

# current directory of git project
gitdir=$(pwd)

# move to the main user directory before running scripts
cd ~
# directory of config files
currentdir=$(pwd)
configdir="$currentdir/.config"

# ----------------------------------------------------------------------------
# end INITIAL VARIABLES

# SETUP
# ----------------------------------------------------------------------------

# define bash config
bashfile="$currentdir/.bashrc";
newbashfile="$gitdir/.bashrc";

# add contents to bash file
sudo tee < $newbashfile -a $bashfile

cp "$gitdir/scripts/screensm.sh" "$currentdir/screensm.sh";
cp "$gitdir/scripts/screenlg.sh" "$currentdir/screenlg.sh";
cp "$gitdir/scripts/isitimportant.sh" "$currentdir/isitimportant.sh";

mkdir "$currentdir/Applications";
# ----------------------------------------------------------------------------
# end SETUP
