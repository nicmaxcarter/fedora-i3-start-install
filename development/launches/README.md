### Development Launch Scripts

This directory is for housing launch scripts that prevent
me from having to navigate to specific directories and run
local servers and such.

This readme file can be deleted, and then your personal git repo can be placed
inside.
