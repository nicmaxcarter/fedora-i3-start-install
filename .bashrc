
## CUSTOM
## -----------------------------------------------------------
alias vim="nvim"
alias phpunit="vendor/bin/phpunit"
alias phinx="vendor/bin/phinx"
alias phpstan="vendor/bin/phpstan"

declare -a pomo_options
pomo_options["work"]="25"
pomo_options["break"]="5"

pomodoro () {
    if [ -n "$1" -a -n "${pomo_options["$1"]}" ]; then
        val=$1
        echo $val | lolcat
        timer ${pomo_options["$val"]}m
        spd-say "'$val' session done"
        notify-send "'$val' session done"
    fi
}

alias wo="pomodoro 'work'"
alias br="pomodoro 'break'"

alias ranger='source ranger'
alias n="pwd | urxvt &"

export term=rxvt-unicode-256color
export TERMINAL=rxvt

alias glog="git log --oneline --decorate --graph --all"

reset_network () {
    nmcli networking off
    sleep 2
    nmcli networking on
}

alias rrnn="reset_network"

screen_lg () {
    xrandr --output displayport-2 --mode 2560x1440
}

screen_sm () {
    xrandr --output displayport-2 --mode 1920x1080
}

alias screenlg="screen_lg"
alias screensm="screen_sm"

# for some of our php apps that have this executable
alias cli="./cli"

# change out default editor to neovim
export editor=nvim;

alias gstatus="gearadmin  --status | sort -n | column -t"

