#!/bin/bash

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# this only works for a user 'mcarter'
# for a different user, change 'mcarter' references in this file
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


# INITIAL VARIABLES
# ----------------------------------------------------------------------------

# current directory of git project
gitdir=$(pwd)

# move to the main user directory before running scripts
cd ~
# directory of config files
currentdir=$(pwd)
configdir="$currentdir/.config"

# ----------------------------------------------------------------------------
# end INITIAL VARIABLES

# ----------------------------------------------------------------------------
# SECTION FIVE - development packages
# ----------------------------------------------------------------------------

# install repo for php 8
sudo dnf -y install php-{cli,fpm,mysqlnd,zip,devel,gd,mbstring,curl,xml,pear,bcmath,json}
sudo dnf -y install php-opcache php-imap php-imagick php-imagick
sudo dnf -y install gearmand php-pecl-gearman
sudo dnf install -y php-soap php-sodium
sudo systemctl start php-fpm
sudo systemctl enable php-fpm

# make usual directories
devdir="$currentdir/development"
mkdir -p "$devdir"
mkdir -p "$devdir/personal"
mkdir -p "$devdir/fun"
mkdir -p "$devdir/flops"
resdir="$devdir/resources"
mkdir -p "$resdir"

# directory for plantuml diagrams
cp -r "$gitdir/development/diagrams" "$devdir/diagrams"

# directory for http requests in AIN, similar to Postman
cp -r "$gitdir/development/http_requests" "$resdir/http_requests"

# directory to house reference files, such as php docs or code cheat sheets
cp -r "$gitdir/development/references" "$resdir/references"

# directory to house often used development launch scripts
cp -r "$gitdir/development/launches" "$resdir/launches"

# directory to house often used development launch scripts
cp -r "$gitdir/Xthemes" "$currentdir/Xthemes"

# directory to house adminer file, similar to PhpMyAdmin
mkdir -p "$resdir/adminer"


# install adminer with styling
cp "$gitdir/development/adminer/adminer.css" "$resdir/adminer/adminer.css"
wget -cO "$resdir/adminer/index.php" https://github.com/vrana/adminer/releases/download/v4.8.1/adminer-4.8.1.php

# install brew package manager
echo -ne '\n' | /bin/bash -c "$(sudo curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo ""
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/mcarter/.bash_profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# install ain http client
brew tap jonaslu/tools
brew install ain

# install timer for pomodoro bash script
brew install caarlos0/tap/timer

# ----------------------------------------------------------------------------
# END SECTION FIVE
# ----------------------------------------------------------------------------

