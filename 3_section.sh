#!/bin/bash

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# this only works for a user 'mcarter'
# for a different user, change 'mcarter' references in this file
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


# INITIAL VARIABLES
# ----------------------------------------------------------------------------

# current directory of git project
gitdir=$(pwd)

# move to the main user directory before running scripts
cd ~
# directory of config files
currentdir=$(pwd)
configdir="$currentdir/.config"

# ----------------------------------------------------------------------------
# end INITIAL VARIABLES

# ----------------------------------------------------------------------------
# SECTION THREE - development environment
# ----------------------------------------------------------------------------

# NEOVIM
# ----------------------------------------------------------------------------
# make a config directory for nvim and make the undo dir
mkdir -p .config/nvim/undodir

# copy nvim config
cp $gitdir/nvim/init.vim ~/.config/nvim/init.vim
cp $gitdir/nvim/coc-settings.json ~/.config/nvim/coc-settings.json

# make php-doc .vim dir
mkdir -p .vim
cp $gitdir/nvim/php-doc.vim ~/.vim/php-doc.vim

# install vimplug for plugins
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# run vimplug
nvim --headless +PlugInstall +qa

# copy resize font
urxvtdir="$currentdir/.urxvt/ext"
sudo mkdir $urxvtdir -p
sudo cp $gitdir/resize-font "$currentdir/.urxvt/ext/resize-font"
# ----------------------------------------------------------------------------
# end NEOVIM

# ----------------------------------------------------------------------------
# END SECTION THREE
# ----------------------------------------------------------------------------
