-- KEYMAPS

-- local opts = { noremap = true, silent = true }
local opts = { noremap = true }

-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' ' -- change the <leader> key to be space
vim.g.maplocalleader = ' '

-- shorter version of Shift+E x
vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

-- replace esc with jk
vim.keymap.set('i', 'jk', '<Esc>')

-- copy to system clipboard
vim.keymap.set('v', '<leader>y', '"+y', opts)
vim.keymap.set('n', '<leader>y', '"+y', opts)
-- paste to system clipboard
vim.keymap.set('v', '<leader>p', '"+p', opts)
vim.keymap.set('n', '<leader>p', '"+p', opts)





-- COME BACK
vim.keymap.set('n', 'n', 'nzzzv', opts) -- COME BACK
-- COME BACK
vim.keymap.set('n', 'N', 'Nzzzv', opts) -- COME BACK
-- COME BACK
vim.keymap.set('n', 'J', 'mzJ`z', opts) -- COME BACK
