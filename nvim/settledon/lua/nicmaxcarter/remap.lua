-- KEYMAPS

-- local opts = { noremap = true, silent = true }
-- local opts = { noremap = true }

vim.g.mapleader = ' ' -- change the <leader> key to be space
vim.g.maplocalleader = ' '

-- shorter version of Shift+E x
vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

-- replace esc with jk
vim.keymap.set('i', 'jk', '<Esc>')

-- copy to system clipboard
vim.keymap.set('v', '<leader>y', '"+y')
vim.keymap.set('n', '<leader>y', '"+y')
-- paste to system clipboard
vim.keymap.set('v', '<leader>p', '"+p')
vim.keymap.set('n', '<leader>p', '"+p')

vim.keymap.set('n', 'n', 'nzzzv') -- iterate down through highlights
vim.keymap.set('n', 'N', 'Nzzzv') -- iterate back through highlights
vim.keymap.set('n', 'J', 'mzJ`z') -- appends the line below
vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<C-u>', '<C-u>zz')

vim.keymap.set('n', 'Q', '<nop>') -- i hate losing where i am at




-- Make adjusting split sizes a bit more friendly
vim.keymap.set('n', '<C-w><C-]>', ':vertical resize +3<CR>')
vim.keymap.set('n', '<C-w><C-[>', ':vertical resize -3<CR>')
vim.keymap.set('n', "<C-w>'", ':resize +3<CR>')
vim.keymap.set('n', "<C-w>;", ':resize -3<CR>')

vim.keymap.set('n', "<leader>fw", ':StripWhitespace<CR>')

vim.keymap.set('n', "<leader>fm", ':Format<CR>', {noremap = true, silent = true})
