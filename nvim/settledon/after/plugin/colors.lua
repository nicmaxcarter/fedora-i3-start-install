function ColorMyPencils(color)
	--color = color or "rose-pine"
	--vim.cmd.colorscheme(color)

	color = color or "gruvbox"
    vim.o.background = "dark"
	vim.cmd.colorscheme(color)
end

ColorMyPencils()
