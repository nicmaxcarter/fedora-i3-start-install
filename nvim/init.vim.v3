set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nowrap
set exrc
set relativenumber
set nu
set hidden
set noerrorbells
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set scrolloff=8
set ignorecase
set fileformat=unix

set colorcolumn=80
hi ColorColumn ctermbg=lightgrey guibg=lightgrey
set signcolumn=yes

set nocompatible

let g:polyglot_disabled = ['php']

let g:neoformat_php_phpcbf = {
      \ 'exe': 'phpcbf',
      \ 'args': [
      \ '--standard=PSR12',
      \ '--extensions=php',
      \ '%',
      \ '||',
      \ 'true'
      \ ],
      \ 'stdin': 1,
      \ 'no_append': 1
      \ }
let g:neoformat_enabled_php = ['phpcbf']

" Enable basic formatting when a filetype is not found
" Enable alignment
let g:neoformat_basic_format_align = 1

" Enable tab to spaces conversion
let g:neoformat_basic_format_retab = 1

" Enable trimming of trailing whitespace
let g:neoformat_basic_format_trim = 1

" only makes neofomrat more chatty
" let g:neoformat_verbose = 1

" Makes all of neovim very chatty
" let &verbose = 1

call plug#begin('~/.vim/plugged')

Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
"Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'branch' : 'master', 'tag' : 'nvim-0.6' }

Plug 'gruvbox-community/gruvbox'
Plug 'itchyny/lightline.vim'

"Plug 'sheerun/vim-polyglot'
"Plug 'nelsyeung/twig.vim'
"Plug 'lumiliet/vim-twig'

Plug 'sbdchd/neoformat'
"Plug 'tokutake/twig-indent'
"Plug 'bronson/vim-trailing-whitespace'
Plug 'ntpeters/vim-better-whitespace'
Plug 'https://github.com/nelsyeung/twig.vim.git'

Plug 'preservim/nerdcommenter'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

" indent lines
Plug 'lukas-reineke/indent-blankline.nvim'
"Still need to install suggestions for files
":CocInstall coc-phpls

call plug#end()

colorscheme gruvbox
highlight Normal guibg=none


let mapleader = " "
"nnoremap <leader>ps :lua require('telescope.builtin').grep_string({ search = vim.fn.input("Grep For > ")})<CR>
nnoremap <leader>ps <cmd>lua require('telescope.builtin').grep_string({ search = vim.fn.input("Grep For > ")})<CR>
nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<CR>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<CR>
inoremap jk <Esc>

" Keeping the cursor centered when navigating search
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

"Undo break points
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap { {<c-g>u
inoremap } }<c-g>u
inoremap ( (<c-g>u
inoremap ) )<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u

"Jumplist mutations, so if you quick jump more than 5 lines, it stores i
"so we can navigate back with ctrl+i/o
nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'

nnoremap <leader>nf :Neoformat<cr>
nnoremap <leader>fw :StripWhitespace<cr>

" for commenting toggles
filetype plugin on

syntax on
filetype on
au BufNewFile,BufRead *.ain set syntax=perl

" this should help with lag
:set lazyredraw
